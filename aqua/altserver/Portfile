# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem          1.0

name                altserver
version             1.5.1
set version_info    [regexp -all -inline {\d+} $version]
set major_version   [lindex $version_info 0]
set minor_version   [lindex $version_info 1]
set patch_version   [lindex $version_info 2]

categories          aqua
license             AGPL-3
platforms           macosx
maintainers         {gmail.com:audvare @Tatsh}
description         A home for apps that push the boundaries of iOS.
long_description    {*}${description}
homepage            https://altstore.io/
master_sites        https://cdn.altstore.io/file/altstore/${name}/

checksums           rmd160  fe5f011649889455085bb902ee1650fd9c6abe01 \
                    sha256  af478c958e3bf9346b59a4a2af4b3120ef0d02fa653b14dab13b7a7ec56d80de \
                    size    6374018

livecheck.type      regex
livecheck.url       https://altstore.io/altserver/sparkle-macos.xml
livecheck.regex     "<title>Version\\ (\\d+\\.\\d+(?:\\.\\d+)?)"

use_zip             yes
extract.mkdir       yes
use_configure       no

distfiles           "${major_version}_${minor_version}_${patch_version}${extract.suffix}"
extract.only        [lindex ${distfiles} 0]

if {${os.major} < 18 || (${os.major} == 17 && ${os.minor} < 4)} {
    pre-fetch {
        ui_error "${name} @${version} requires macOS 10.14.4 or later."
        return -code error "incompatible macOS version"
    }
}

build {}

destroot {
    copy "${worksrcpath}/AltServer.app" ${destroot}${applications_dir}
}
