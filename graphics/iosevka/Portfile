# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem          1.0
PortGroup           github 1.0

github.setup            be5invis Iosevka 16.3.6 v
github.livecheck.regex  {([0-9.]+)}
github.tarball_from     releases

name                iosevka
categories          graphics
license             OFL-1.1
platforms           macosx
maintainers         {gmail.com:audvare @Tatsh}
description         Fixed width minimal font with ligatures.
long_description    Iosevka is an open-source, sans-serif + slab-serif, \
                    monospace + quasi‑proportional typeface family, designed \
                    for writing code, using in terminals, and preparing \
                    technical documents.
homepage            https://typeof.net/Iosevka/

distfiles           ttf-${name}-${version}.zip

checksums           rmd160  21ff9d06779ec132a81ec1c113c77ffbdcc5e7bc \
                    sha256  58805b630f03eb635c95e84bd5253df8b5a20cb58b2522e9ff66a51c868fd10a \
                    size    90916269

worksrcdir
use_configure           no
use_zip                 yes

destroot.violate_mtree  yes

build {}

destroot {
    xinstall -d -m 755 ${destroot}/Library/Fonts
    foreach file [glob -nocomplain -directory ${worksrcpath} *.ttf] {
        set basename [lrange [split ${file} /] end end]
        copy ${file} ${destroot}/Library/Fonts/${basename}
    }
}

subport ${name}-term {
    distfiles ttf-${name}-term-${version}.zip
    checksums           rmd160  d6013409e894de6c93e33af283ff2b43542236c2 \
                        sha256  14f862ca69d7728909f8092d62f705c6cdfb3b40d637deab473e2bda68665f7a \
                        size    90859190
}

subport ${name}-fixed {
    distfiles ttf-${name}-fixed-${version}.zip
    checksums           rmd160  c0213fad2188b7ccc6b0b78e84a78e98cb0a1145 \
                        sha256  28ca946608e815459f314aaf83daa2991a39b5bdf3e96bb249f95f9b4a65dbd1 \
                        size    72850667
}
